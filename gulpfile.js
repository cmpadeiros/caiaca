var gulp = require('gulp');
var less = require('gulp-less');
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var concat = require('gulp-concat');

// Set the banner content
var banner = ['/*!\n',
    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license.type %> (<%= pkg.license.url %>)\n',
    ' */\n',
    ''
].join('');

// Compile LESS files from /less into /css
gulp.task('less', function() {
    return gulp.src('less/style.less')
        .pipe(less())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulp.dest('dev/css'))
});

// Minify compiled CSS
gulp.task('minify-css', ['less'], function() {
    return gulp.src('dev/css/*.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('css'))
});

// Minify template JS
gulp.task('minify-template-js', function() {
    return gulp.src(['dev/js/creative.js'])
        .pipe(uglify())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('js'))
});

// Minify our JS
gulp.task('minify-our-js', function() {
    return gulp.src(['dev/js/countries.js', 'dev/js/map.js', 'dev/js/main.js'])
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dev/js'))
});



gulp.task('concat', function() {
  return gulp.src(['dev/js/countries.min.js','dev/js/map.min.js','dev/js/main.min.js'])
    .pipe(concat('page.min.js'))
    .pipe(gulp.dest('js'));
});

// Run everything
gulp.task('default', [
    'less', 'minify-css', 'minify-template-js', 'minify-our-js', 'concat'
]);


