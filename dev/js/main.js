function adjust_map_container_height() {
    $("#map").css("height", $("#main-factory-img").height());
}

$(document).ready(function () {

    var bag_icon = myMap.createIcon('img/icons/bag.png', 30, 30);
    var language = $("html").attr("lang");

    $('.img-wait').imagesLoaded(function () {
        adjust_map_container_height();

        myMap.initMap(
            countries.Portugal.latitude - 4.0,
            countries.Portugal.longitude,
            2,
            "map"
        );
        myMap.invalidateSizeOfMap();
        myMap.disableScrollWithMouseWheel();
        myMap.populateMapWithCountryMarkers(bag_icon, language);

    });

    $(window).on("orientationchange resize pageshow", function () {
        adjust_map_container_height();
        myMap.invalidateSizeOfMap();
    });

});