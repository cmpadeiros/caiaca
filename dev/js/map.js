/* Module containing the map logic for the page */

var myMap = {
    map_instance: {},
    map_markers: [],

    /*
     Creates a leaflet map instance, initialized with the given parameters
     @param initial_latitude: float, initial latitude value for centering the map
     @param initial_longitude: float, initial longitude value for centering the map
     @param zoom: float, initial zoom level for the map
     @param map_element_id: The id of the HTML element containing the map
     */
    initMap: function initMap(initial_latitude, initial_longitude, zoom, map_element_id) {
        // set up the map

        var southwest = L.latLng(-90.0, -180.0),
            northeast = L.latLng(90.0, 180.0),
            bounds = L.latLngBounds(southwest, northeast);

        this.map_instance = L.map(map_element_id,
            {
                maxBounds: bounds,
                maxBoundsViscosity: 1
            });

        this.map_instance.setView(
            [initial_latitude, initial_longitude], zoom);

        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            minZoom: 1.6,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(this.map_instance);


    },

    /*
     Marks a point in the map
     */

    addMarker: function (latitude, longitude, icon) {
        return L.marker([latitude, longitude], {icon: icon}).addTo(this.map_instance);
    },

    /*
     Creates an map icon from the given image, to use as a map marker
     */
    createIcon: function (path_to_image, icon_width, icon_height) {
        return L.icon({
            iconUrl: path_to_image,
            iconSize: [icon_width, icon_height], // size of the icon
            iconAnchor: [Math.round(icon_width / 2), Math.round(icon_height / 2)] // point of the icon which will correspond to marker's location
        });
    },

    disableScrollWithMouseWheel: function () {
        this.map_instance.scrollWheelZoom.disable();
    },

    invalidateSizeOfMap: function () {
        this.map_instance.invalidateSize();
    },

    populateMapWithCountryMarkers: function (bag_icon, language) {

        for (var key in countries) {
            if (countries.hasOwnProperty(key)) {

                var country = countries[key];
                var marker = myMap.addMarker(
                    country["latitude"],
                    country["longitude"],
                    bag_icon
                );

                if(language == "pt"){
                    country_name = key;
                }
                else if (language == "en"){
                    country_name = country["en_name"];
                }

                marker.bindPopup("<strong>" + country_name + "</strong>");

                marker.on('mouseover', function (e) {
                    this.openPopup();
                });
                marker.on('mouseout', function (e) {
                    this.closePopup();
                });
            }
        }
    }
};