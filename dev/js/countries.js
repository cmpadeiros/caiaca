var countries = {
    "Portugal": {
        en_name: "Portugal",
        latitude: 38.7166700,
        longitude: -9.1333300
    },
    "Espanha": {
        en_name: "Spain",
        latitude: 40.4165000,
        longitude: -3.702560
    },
    "França": {
        en_name: "France",
        latitude: 48.8534100,
        longitude: 2.3488000
    },
    "Holanda": {
        en_name: "Netherlands",
        latitude: 52.3740300,
        longitude: 4.8896900
    },
    "Reino Unido": {
        en_name: "United Kingdom",
        latitude: 51.5085300,
        longitude: -0.1257400
    },
    "Itália": {
        en_name: "Italy",
        latitude: 41.8919300,
        longitude: 12.5113300
    },
    "Chipre": {
        en_name: "Cyprus",
        latitude: 35.1753100,
        longitude: 33.3642000
    },
    "Angola": {
        en_name: "Angola",
        latitude: -8.8368200,
        longitude: 13.2343200
    },
    "Tenerife": {
        en_name: "Tenerife",
        latitude: 28.2916,
        longitude: -16.6291
    }


};
